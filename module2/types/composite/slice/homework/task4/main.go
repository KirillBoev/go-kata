package main

import "fmt"

type User struct {
	Name string
	Age  int
}

func main() {
	users := []User{
		{
			Name: "Eva",
			Age:  13,
		},
		{
			Name: "Victor",
			Age:  28,
		},
		{
			Name: "Dex",
			Age:  34,
		},
		{
			Name: "Billy",
			Age:  21,
		},
		{
			Name: "Foster",
			Age:  29,
		},
	}

	subUsers := users[2:]
	subUsers = editSecondSlice(subUsers)
	fmt.Println(subUsers)
	fmt.Println(users)
}

func editSecondSlice(users []User) []User {
	subUsers := make([]User, len(users))
	copy(subUsers, users)
	for i := range subUsers {
		subUsers[i].Name = "unknown"
	}
	return subUsers
}
