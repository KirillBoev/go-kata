package main

import "fmt"

type Project struct {
	Name  string
	Stars int
}

func main() {
	projects := []Project{
		{
			Name:  "https://github.com/docker/compose",
			Stars: 27600,
		},
		{
			Name:  "https://github.com/hpcaitech/ColossalAI",
			Stars: 23700,
		},
		{
			Name:  "https://github.com/binary-husky/chatgpt_academic",
			Stars: 13800,
		},
		{
			Name:  "https://github.com/Lightning-AI/lit-llama",
			Stars: 1300,
		},
		{
			Name:  "https://github.com/Yidadaa/ChatGPT-Next-Web",
			Stars: 5800,
		},
		{
			Name:  "https://github.com/Bin-Huang/chatbox",
			Stars: 4000,
		},
		{
			Name:  "https://github.com/davincifans101/pinduoduo_backdoor_detailed_report",
			Stars: 972,
		},
		{
			Name:  "https://github.com/davinci1012/pinduoduo_backdoor_unpacker",
			Stars: 989,
		},
		{
			Name:  "https://github.com/showlab/Tune-A-Video",
			Stars: 2300,
		},
		{
			Name:  "https://github.com/Kent0n-Li/ChatDoctor",
			Stars: 1300,
		},
		{
			Name:  "https://github.com/nomic-ai/gpt4all",
			Stars: 11600,
		},
		{
			Name:  "https://github.com/TaxyAI/browser-extension",
			Stars: 2200,
		},
		{
			Name:  "https://github.com/ZrrSkywalker/LLaMA-Adapter",
			Stars: 934,
		},
	}

	projectsMap := make(map[string]Project)
	for i := range projects {
		projectsMap[projects[i].Name] = projects[i]
	}

	for _, value := range projectsMap {
		fmt.Println("Value:", value)
	}
}
