package main

import (
	"fmt"
	"unsafe"
)

func main() {
	typeFloat()
}

func typeFloat() {
	fmt.Println("===START type uint===")
	var floatNumber float32
	var uintNumber uint32 = 1 << 29
	uintNumber += 1 << 28
	uintNumber += 1 << 27
	uintNumber += 1 << 26
	uintNumber += 1 << 25
	uintNumber += 1 << 21
	uintNumber += 1 << 31

	floatNumber = *(*float32)(unsafe.Pointer(&uintNumber))
	fmt.Println(floatNumber)
	a, b := 2.3329, 3.1234
	c := a + b
	fmt.Println("Пример ошибки 1:", c)

	a = 9.99999
	b2 := float64(a)
	fmt.Println("Пример ошибки 2:", b2)

	//a = 999998455
	//b3 := float32(a)
	//fmt.Printf("Пример ошибки 3:", "%f\n", b3)

	a4 := 5.2
	b4 := 4.1
	fmt.Println(a4 + b4)
	fmt.Println((a4 + b4) == 9.3)

	c4 := 5.2
	d4 := 2.1
	fmt.Println(c4 + d4)
	fmt.Println((a4 + b4) == 7.3)

	fmt.Println("===END type uint===")
}
