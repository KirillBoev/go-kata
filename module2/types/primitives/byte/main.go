package main

import (
	"fmt"
	"unsafe"
)

func main() {
	typeByte()
}

func typeByte() {
	var b byte = 124
	fmt.Println("Размер в байтах:", unsafe.Sizeof(b))
}
