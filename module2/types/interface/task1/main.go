package main

import (
	"fmt"
)

type MyInterface interface{}

func main() {
	var n *int
	fmt.Println(n == nil)
	test(n)
}

func test(r interface{}) {
	switch r.(type) {
	case nil:
		fmt.Println("Unsuccess!")
	case *int:
		if r != nil {
			fmt.Println("Success!")
		}
	default:
		fmt.Println("Another type")
	}
}
