module gitlab.com/KirillBoev/go-kata

go 1.18

require (
	gitlab.com/KirillBoev/greet v0.0.0-20230413154808-841963db0523 // indirect
	github.com/json-iterator/go v1.1.12
	github.com/ptflp/godecoder v0.0.0-20210911155149-6beba132b443
)
